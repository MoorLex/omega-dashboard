# Omega Dashboard

Omega Dashboard is a set of modern professional Vue based templates. It's a powerful and super flexible tool, which 
suits best for any kind of web application: Web Applications, CRM, CMS, Admin Panels, Dashboards, etc. 
Omega Dashboard is fully responsive, which means that it looks perfect on mobiles and tablets.

Also, this is a framework, which uses the concept of modularity. The use of such approach allows you to drastically 
reduce the time and cost of development. Build the interface from the ready blocks and modules quickly and easily, 
like Lego! And all of this is available now for Vue.

Omega Dashboard is a completely modular framework, built on the latest technologies, such as BEM methodology 
(Block Element Modifier). Following the simple and understandable rules of this methodology allows the easy support of 
code even after long-term development. Adding new functionality and modifying the old one has never been so easy before. 
The presence of a variety of useful development methodologies make this process easier and more enjoyable; 
for example, such feature as rem measurement units make mobile versions of applications, built on the basis 
of Omega Dashboard, look great on any type of devices. Open our template on your mobile phone and be sure to 
verify it yourself!

## Build Setup

``` bash
# install dependencies
npm

# serve with hot reload at localhost:8080
npm dev

# build for production with minification
npm build

# build for production and view the bundle analyzer report
npm build --report

# run unit tests
npm unit

# run all tests
npm test
```
