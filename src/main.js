import Vue from 'vue'
import App from './App'
import router from './router'

import Omega from './omega'
Vue.use(Omega);

Vue.config.productionTip = false;

// eslint-disable-next-line
let vue = new Vue({
  el: '#app',
  router,
  data() {
    return {
      year: '2019',
      version: 2.1,
      state: undefined
    }
  },
  render: h => h(App),
  components: { App },
  template: '<App/>'
});
