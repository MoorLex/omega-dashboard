import Vue from 'vue'
import Router from 'vue-router'

const Admin = () => import('./templates/admin');
const Clear = () => import('./templates/clear');

const Cards = () => import('./templates/pages/Cards');
const Buttons = () => import('./templates/pages/Buttons');
const Alerts = () => import('./templates/pages/Alerts');
const Icons = () => import('./templates/pages/Icons');
const Stickers = () => import('./templates/pages/Stickers');
const Slider = () => import('./templates/pages/Slider');
const Typography = () => import('./templates/pages/Typography');
const Form = () => import('./templates/pages/Form');
const Charts = () => import('./templates/pages/Charts');
const Modal = () => import('./templates/pages/Modal');
const Navbar = () => import('./templates/pages/Navbar');

const Dashboard = () => import('./templates/pages/Dashboard');
const Blank = () => import('./templates/pages/Blank');
const Files = () => import('./templates/pages/Files');
const Error_404 = () => import('./templates/pages/Error_404');
const Maintenance = () => import('./templates/pages/Maintenance');
const Login = () => import('./templates/pages/Login');
const Login_2 = () => import('./templates/pages/Login_2');
const Inbox = () => import('./templates/pages/Inbox');

Vue.use(Router);

let router = new Router({
  linkActiveClass: 'active',
  hashbang: false,
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Admin,
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/cards',
          name: 'Cards',
          component: Cards
        },
        {
          path: '/buttons',
          name: 'Buttons',
          component: Buttons
        },
        {
          path: '/icons',
          name: 'Icons',
          component: Icons
        },
        {
          path: '/alerts',
          name: 'Alerts',
          component: Alerts
        },
        {
          path: '/pages/blank',
          name: 'Blank',
          component: Blank
        },
        {
          path: '/pages/files',
          name: 'Files',
          component: Files
        },
        {
          path: '/stickers',
          name: 'Stickers',
          component: Stickers
        },
        {
          path: '/slider',
          name: 'Slider',
          component: Slider
        },
        {
          path: '/typography',
          name: 'Typography',
          component: Typography
        },
        {
          path: '/form',
          name: 'Form',
          component: Form
        },
        {
          path: '/charts',
          name: 'Charts',
          component: Charts
        },
        {
          path: '/modal',
          name: 'Modal',
          component: Modal
        },
        {
          path: '/navbar',
          name: 'Navbar',
          component: Navbar
        },
        {
          path: '/pages/404',
          name: '404',
          component: Error_404
        },
        {
          path: '/pages/maintenance',
          name: 'Maintenance',
          component: Maintenance
        },
        {
          path: '/pages/inbox',
          name: 'Inbox',
          component: Inbox
        }
      ]
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/login_2',
      component: Login_2
    },
    {
      path: '/recovery',
      component: Clear
    }
  ],
  scrollBehavior (to, from, savedPosition)
  {
    if (savedPosition) return savedPosition;
    else return { x: 0, y: 0 }
  }
});

// router.beforeEach((to, from, next) => {
//
//   if(!to.matched.some(record => record.meta.guest)) {
//
//     if(localStorage.getItem('token')) next();
//     else next({ name: 'Login'});
//
//   } else next();
// });


export default router;
