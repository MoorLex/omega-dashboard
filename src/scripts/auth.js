import Vue from 'vue'

let auth = new Vue({
  data() {
    return {
      user: {},
      success: false
    }
  },
  computed: {
    id: {
      get: function() {
        return localStorage.getItem('user_id');
      },
      set: function(user_id) {
        localStorage.setItem('user_id', user_id);
      }
    },
    token: {
      get: function() {
        return localStorage.getItem('token');
      },
      set: function(id_token) {
        localStorage.setItem('token', id_token);
      }
    },
    expiresAt: {
      get: function() {
        return localStorage.getItem('expires_at')
      },
      set: function(expiresIn) {
        let expiresAt = JSON.stringify(expiresIn * 1000 + new Date().getTime())
        localStorage.setItem('expires_at', expiresAt)
      }
    }
  },
  methods: {

    getUser(callback) {
      Vue.axios.defaults.headers.common['x-access-token'] = this.token;

      this.$api.v1.get('/profile ')
        .then(response => {
          this.success = true;

          this.user = {
            email: response.data.profile['email']
          };

          if(callback && callback.success) callback.success(response.data)
        })
        .catch(error => { if(callback && callback.error) callback.error(error) })
    },

    login(data, rem, callback) {
      this.$api.v2.post('/session', data)
        .then(response => {
          if(rem)
          {
            this.token = response.data.token;
            Vue.axios.defaults.headers.common['x-access-token'] = response.data.token;
          }

          this.user = {
            email: response.data.profile['email']
          };

          if(callback && callback.success) callback.success(response.data);
          this.success = true;
        })
        .catch(error => { if(callback && callback.error) callback.error(error) })
    },

    recovery(data, callback) {
      this.$api.v1.get('/password', {params: data})
        .then(response => {
          if(callback && callback.success) callback.success(response.data)
        })
        .catch(error => { if(callback && callback.error) callback.error(error) })
    },

    logOut() {
      localStorage.removeItem('token');
      this.success = false;
      delete Vue.axios.defaults.headers.common['x-access-token'];
      this.$api.v2.delete('/session');
      location.replace('/');
    }
  }
});

export default {
  install: function(Vue) {
    Vue.prototype.$auth = auth
  }
}
