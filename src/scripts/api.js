import axios from 'axios'

export default {
  install: function(Vue) {
    Vue.prototype.$api = {
      v1: axios.create({
        baseURL: 'https://arapi.a3technology.ru/v1/admin'
      }),

      v2: axios.create({
        baseURL: 'https://arapi.a3technology.ru/v2/admin'
      })
    }
  }
}
