import PerfectScrollbar from 'vue2-perfect-scrollbar';

export default {
  install(Vue) {

    Vue.use(PerfectScrollbar);
  }
}
