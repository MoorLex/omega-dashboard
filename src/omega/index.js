import Vendors from './vendors'

import Menu from './modules/menu'

import MegaCard from './components/card'
import MegaAlert from './components/alert'
import MegaHeader from './components/header'
import MegaButton from './components/button'
import MegaImage from './components/image'
import MegaModal from './components/modal'
import MegaMenuLink from './components/menu-link'
import MegaMenuGroup from './components/menu-group'

import MegaInput from './components/form-input'
import MegaMask from './components/form-mask'
import MegaSelect from './components/form-select'
import MegaTextarea from './components/form-textarea'
import MegaSwitch from './components/form-switch'
import MegaNumber from './components/form-number'
import MegaRange from './components/form-range'
import MegaProgress from './components/progress'
import MegaDropZone from './components/drop-zone'

import MegaSlider from './components/slider'
import MegaSlide from './components/slide'
import MegaChart from './components/chart'
import MegaChartSet from './components/chart-set'
import MegaProgressRing from './components/progress-ring'

const Omega = {
  install (Vue) {

    Vendors.install(Vue);

    Vue.prototype.$mega = {
      menu: Menu
    }

    Vue.component('mega-card', MegaCard);
    Vue.component('mega-alert', MegaAlert);
    Vue.component('mega-header', MegaHeader);
    Vue.component('mega-button', MegaButton);
    Vue.component('mega-image', MegaImage);
    Vue.component('mega-modal', MegaModal);
    Vue.component('mega-menu-link', MegaMenuLink);
    Vue.component('mega-menu-group', MegaMenuGroup);

    Vue.component('mega-input', MegaInput);
    Vue.component('mega-mask', MegaMask);
    Vue.component('mega-select', MegaSelect);
    Vue.component('mega-textarea', MegaTextarea);
    Vue.component('mega-switch', MegaSwitch);
    Vue.component('mega-range', MegaRange);
    Vue.component('mega-number', MegaNumber);
    Vue.component('mega-progress', MegaProgress);
    Vue.component('mega-drop-zone', MegaDropZone);

    Vue.component('mega-slider', MegaSlider);
    Vue.component('mega-slide', MegaSlide);
    Vue.component('mega-chart', MegaChart);
    Vue.component('mega-chart-set', MegaChartSet);
    Vue.component('mega-progress-ring', MegaProgressRing);
  }
}

export default Omega
